import React, { lazy, Suspense } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { AnimatePresence, motion } from 'framer-motion';

import { ThemeProvider } from '@material-ui/styles';

import MuiTheme from './theme';

// Layout Blueprints

import { LeftSidebar, PresentationLayout } from './layout-blueprints';

// Example Pages

import Buttons from './example-pages/Buttons';
import Dropdowns from './example-pages/Dropdowns';
import NavigationMenus from './example-pages/NavigationMenus';
import ProgressBars from './example-pages/ProgressBars';
import Pagination from './example-pages/Pagination';
import Scrollable from './example-pages/Scrollable';
import Badges from './example-pages/Badges';
import Icons from './example-pages/Icons';
import UtilitiesHelpers from './example-pages/UtilitiesHelpers';
import RegularTables1 from './example-pages/RegularTables1';
import RegularTables4 from './example-pages/RegularTables4';
import FormsControls from './example-pages/FormsControls';

import DashboardDefault from './example-pages/DashboardDefault';
import AgentList from './page/agent/AgentList';
import AgentDetail from './page/agent/DetailAgent';
import SettingBenner from './page/Pengaturan/settingBenner';
import SettingPoin from './page/Pengaturan/settingPoin';
import SettingTNC from './page/Pengaturan/settingTNC';
import SettingFAQ from './page/Pengaturan/settingFAQ';
import ApproveEnchange from './page/change_point/approve_enchange';
import TransferStatus from './page/change_point/tranferStatus';
import Cards3 from './example-pages/Cards3';
import LandingPage from './example-pages/LandingPage';
import Accordions from './example-pages/Accordions';
import Modals from './example-pages/Modals';
import Notifications from './example-pages/Notifications';
import Popovers from './example-pages/Popovers';
import Tabs from './example-pages/Tabs';
import ApexCharts from './example-pages/ApexCharts';
import Maps from './example-pages/Maps';
import ListGroups from './example-pages/ListGroups';

const Routes = ({ match }) => {
  console.log(match);
  const location = useLocation();

  const pageVariants = {
    initial: {
      opacity: 0,
      scale: 0.99
    },
    in: {
      opacity: 1,
      scale: 1
    },
    out: {
      opacity: 0,
      scale: 1.01
    }
  };

  const pageTransition = {
    type: 'tween',
    ease: 'anticipate',
    duration: 0.4
  };

  return (
    <ThemeProvider theme={MuiTheme}>
      {/* <AnimatePresence> */}
      {/* <Suspense
          fallback={
            <div className="d-flex align-items-center vh-100 justify-content-center text-center font-weight-bold font-size-lg py-3">
              <div className="w-50 mx-auto">Please wait loading!!!!</div>
            </div>
          }> */}
      <Switch>
        <LeftSidebar>
          <Route
            path={`${match.url}/agent/list`}
            component={DashboardDefault}
          />
          <Route
            path={`${match.url}/agent/detail/:id`}
            component={AgentDetail}
          />
          <Route
            path={`${match.url}/inchange/approve`}
            component={ApproveEnchange}
          />
          <Route
            path={`${match.url}/inchange/transfer`}
            component={TransferStatus}
          />
          <Route path={`${match.url}/setting/poin`} component={SettingPoin} />
          <Route
            path={`${match.url}/setting/benner`}
            component={SettingBenner}
          />
          <Route
            path={`${match.url}/setting/input`}
            component={FormsControls}
          />
          <Route path={`${match.url}/setting/tnc`} component={SettingTNC} />
          <Route path={`${match.url}/setting/faq`} component={SettingFAQ} />
          <Redirect exact to={`${match.url}/agent/list`} />

          {/* <Redirect from={`/user/login`} to={`${match.url}/agent/list`} /> */}
        </LeftSidebar>
      </Switch>
      {/* </Suspense> */}
      {/* </AnimatePresence> */}
    </ThemeProvider>
  );
};

export default Routes;
