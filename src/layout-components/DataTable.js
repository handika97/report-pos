// author https://gitlab.com/riyan.irawan
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import {
  TableBody,
  Table,
  TableCell,
  TableContainer,
  TableRow,
  IconButton,
  Paper,
  Grid,
  Input,
  CircularProgress,
  TableHead
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

import { useTheme } from '@material-ui/core/styles';

import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const useStyles1 = makeStyles(theme => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5)
  }
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = event => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = event => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = event => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = event => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page">
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page">
        {theme.direction === 'rtl' ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page">
        {theme.direction === 'rtl' ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page">
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired
};

const useStyles2 = makeStyles({
  table: {
    minWidth: 500
  }
});

export default function DataTable({
  headers = [],
  rows = [],
  rowsPerPage = 5,
  page = 0,
  count = 0,
  renderRow = (d, i) => <></>,
  pageLengthList = [5, 10, 25, 100],
  handleChangePage = (event, newPage) => {},
  handleChangeRowsPerPage = event => {},
  renderHeaderActions = <></>,
  searchProps = {},
  renderHeader = h => (
    <TableCell
      style={{
        backgroundColor: 'teal',
        color: '#fff'
      }}>
      {h}
    </TableCell>
  ),
  paginationColSpan = null,
  isLoading = false
}) {
  const classes = useStyles2();
  return (
    <Fragment>
      <Grid container style={{ marginBottom: '10px' }}>
        <Grid item md={10}>
          {renderHeaderActions}
        </Grid>
        <Grid item md={2}>
          <Input
            fullWidth
            placeholder="Cari..."
            inputMode="text"
            onChange={e => {
              alert(e.target.value);
            }}
            {...searchProps}
          />
        </Grid>
      </Grid>
      <TableContainer component={Paper}>
        {isLoading ? (
          <div
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              alignContent: 'center',
              textAlign: 'center'
            }}>
            <CircularProgress color="secondary" />
            <br />
            <span>Loading...</span>
          </div>
        ) : (
          <Table
            className={classes.table}
            aria-label="custom pagination table table-striped">
            <TableHead>
              <TableRow>{headers.map(h => renderHeader(h))}</TableRow>
            </TableHead>
            <TableBody>{rows.map((row, i) => renderRow(row, i))}</TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={pageLengthList}
                  colSpan={
                    paginationColSpan == null
                      ? headers.length
                      : paginationColSpan
                  }
                  count={count}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: { 'aria-label': 'rows per page' },
                    native: true
                  }}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        )}
      </TableContainer>
    </Fragment>
  );
}
