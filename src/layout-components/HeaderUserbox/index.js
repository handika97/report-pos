import React, { Fragment } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Avatar,
  Box,
  Menu,
  Button,
  List,
  ListItem,
  Tooltip,
  Divider,
} from "@material-ui/core";
import { Logout } from "../../redux/features/authSlice";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import avatar5 from "../../assets/images/avatars/avatar5.jpg";
export default function HeaderUserbox() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  let dispatch = useDispatch();
  let history = useHistory();
  const auth = useSelector((state) => state.auth);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const Keluar = async () => {
    await dispatch(Logout());
  };

  return (
    <Fragment>
      <Button
        color="inherit"
        onClick={handleClick}
        className="text-capitalize px-3 text-left btn-inverse d-flex align-items-center"
      >
        <div className="d-grid d-xl-block pl-3">
          <div className="font-weight-bold pt-2 line-height-1 m-3">
            hiii, {auth.data.firstName}
          </div>
        </div>
        <Box>
          <Avatar sizes="44" alt="Emma Taylor" src={avatar5} />
        </Box>
        <span className="pl-1 pl-xl-3">
          <FontAwesomeIcon icon={["fas", "angle-down"]} className="opacity-5" />
        </span>
      </Button>

      <Menu
        anchorEl={anchorEl}
        keepMounted
        getContentAnchorEl={null}
        open={Boolean(anchorEl)}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "center",
          horizontal: "center",
        }}
        onClose={handleClose}
        className="ml-2"
      >
        <div className="dropdown-menu-right dropdown-menu-lg overflow-hidden p-0">
          <List className="text-left bg-transparent d-flex align-items-center flex-column pt-0">
            <Box>
              <Avatar sizes="44" alt="Emma Taylor" src={avatar5} />
            </Box>
            <div className="pl-3  pr-3">
              <div className="font-weight-bold text-center pt-2 line-height-1 text-capitalize ">
                {auth.data.name}
              </div>
              <span className="text-black-50 text-center text-capitalize ">
                {auth.data.role}
              </span>
            </div>
            <Divider className="w-100 mt-2" />
            <ListItem
              button
              onClick={() => {
                Keluar();
              }}
            >
              Keluar
            </ListItem>
          </List>
        </div>
      </Menu>
    </Fragment>
  );
}
