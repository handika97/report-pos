import React, { Fragment } from "react";

import clsx from "clsx";
import { Link } from "react-router-dom";

import { IconButton, Box } from "@material-ui/core";

import projectLogo from "../../assets/images/react.svg";

const HeaderLogo = (props) => {
  return (
    <Fragment>
      <div className={clsx("app-header-logo", {})}>
        <Box className="header-logo-wrapper" title="Simple POS">
          <Link to="/app/agent/list" className="header-logo-wrapper-link">
            <IconButton
              color="primary"
              size="medium"
              className="header-logo-wrapper-btn"
            >
              <img
                className="app-header-logo-img"
                alt="Simple POS"
                src={require("../../assets/images/logo_app.jpg")}
              />
            </IconButton>
          </Link>
          <Box className="header-logo-text">Simple POS</Box>
        </Box>
      </div>
    </Fragment>
  );
};

export default HeaderLogo;
