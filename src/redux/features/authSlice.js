import { createSlice } from "@reduxjs/toolkit";
import { post, patch } from "./slice";
import { BaseUrl } from "../../utilities/BaseUrl";
export const auth = createSlice({
  name: "auth",
  initialState: {
    isLogin: false,
    token: "",
    data: { name: "", nik: "", role: "", divre: "" },
    entry: 0,
    exit: 0,
    remember: { status: false, Username: "", Password: "" },
    reset: false,
    notif: 0,
    notifVerify: 0,
  },
  reducers: {
    startAsync: (state) => {
      state.loading = true;
    },
    stopAsync: (state) => {
      state.loading = false;
    },
    loginSuccess: (state, action) => {
      state.isLogin = true;
      state.token = action.payload.token;
      state.data = action.payload;
    },
    logout: (state) => {
      state.isLogin = false;
      state.token = "";
      state.data = { name: "", role: "", token: "", username: "" };
      state.exit = 0;
      state.entry = 0;
      state.reset = false;
    },
    countEntryExit: (state, action) => {
      state.exit = action.payload.exit;
      state.entry = action.payload.entry;
      state.reset = false;
    },
    getAgain: (state) => {
      state.reset = true;
    },
    setNotif: (state, action) => {
      state.notif = action.payload;
    },
    setNotifVerify: (state, action) => {
      state.notifVerify = action.payload;
    },
    setRemember: (state, action) => {
      state.remember = {
        status: action.payload.status,
        Username: action.payload.username,
        Password: action.payload.password,
      };
    },
  },
});

export const {
  startAsync,
  stopAsync,
  loginSuccess,
  logout,
  countEntryExit,
  getAgain,
  setNotif,
  setRemember,
  setNotifVerify,
} = auth.actions;

export default auth.reducer;

// ---------------- ACTION ---------------

// import {errorMessage} from '../../utils';
const defaultBody = null;

export const Login = (history, Username, Password, actionsErr = () => {}) => (
  dispatch
) => {
  dispatch(
    post(
      "/api/users/login",
      { username: Username, password: Password },
      (res) => {
        dispatch(loginSuccess(res.data.data));
        history.replace("/app/report/menu");
      },
      (err) => {
        actionsErr(err);
      }
    )
  );
};
export const Logout = () => (dispatch) => {
  dispatch(logout());
};
