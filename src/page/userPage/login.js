import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import Axios from "axios";
import { FiRefreshCw } from "react-icons/fi";
import {
  Login,
  loginSuccess,
  setRemember,
} from "../../redux/features/authSlice";
import { get, post } from "../../redux/features/slice";
import User from ".";
import {
  TextField,
  Grid,
  IconButton,
  Checkbox,
  FormControlLabel,
  InputAdornment,
} from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { encode, decode } from "js-base64";
const PasswordField = ({ value, onChange = () => {}, placeholder }) => {
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);
  return (
    <TextField
      fullWidth
      id="input-with-icon-grid"
      type={showPassword ? "text" : "password"}
      variant="outlined"
      size="small"
      value={value}
      placeholder={placeholder}
      onChange={(e) => onChange(e.target.value)}
      style={{ backgroundColor: "#00a4b3", borderRadius: 5 }}
      InputProps={{
        endAdornment: (
          <InputAdornment position="start" style={{ width: 20 }}>
            <IconButton
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              onMouseDown={handleMouseDownPassword}
            >
              {showPassword ? (
                <Visibility style={{ fontSize: 17 }} />
              ) : (
                <VisibilityOff style={{ fontSize: 17 }} />
              )}
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
  );
};

const LoginPage = ({ match }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  console.log(auth);
  const [Password, setPassword] = useState(
    auth.remember.status ? decode(auth.remember.Password) : ""
  );
  const [Username, setUsername] = useState(
    auth.remember.status ? decode(auth.remember.Username) : ""
  );
  const [ErrorMSG, setErrorMSG] = useState("");
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [modal, setModal] = useState(false);
  const [Loading, setLoading] = useState(false);
  const [Checked, setChecked] = useState(
    auth.remember?.status ? auth.remember.status : false
  );

  const LoginActions = (e) => {
    e.preventDefault();
    if (Password && Username) {
      dispatch(
        setRemember({
          status: Checked,
          username: Checked ? encode(Username) : "",
          password: Checked ? encode(Password) : "",
        })
      );
      dispatch(
        Login(history, Username, Password, (err) => {
          console.log(err.response.data);
          setStatus("Error");
          setMessage(true);
          setLoading(false);
          // getData();
          setErrorMSG(
            err.response?.data.message
              ? err.response.data.message
              : "Opps Something Error"
          );
          setTimeout(() => setMessage(false), 3000);
        })
      );
    } else {
      if (!Password) {
        setErrorMSG("Masukan Password Anda");
      }
      if (!Username) {
        setErrorMSG("Masukan Username Anda");
      }
    }
  };

  return (
    <Fragment>
      <Wrapper>
        <div className="main-container">
          <div className="form-container">
            <div className="srouce"></div>

            <div className="form-body">
              <div className="logo">
                <img
                  src={require("../../assets/images/logo_app.jpg")}
                  alt="Italian Trulli"
                  style={{
                    height: 80,
                    width: "70%",
                    objectFit: "contain",
                  }}
                />
              </div>
              <form action="" className="the-form" onSubmit={LoginActions}>
                <label htmlFor="email">Username / No. HP</label>
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      fullWidth
                      id="input-with-icon-grid"
                      // label="Search"
                      variant="outlined"
                      size="small"
                      value={Username}
                      onChange={(e) => setUsername(e.target.value)}
                      placeholder="Enter your username"
                    />
                  </Grid>
                </Grid>
                <label htmlFor="password">Password / PIN</label>
                <PasswordField
                  value={Password}
                  onChange={(e) => setPassword(e)}
                  placeholder="Enter your password"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={Checked}
                      onChange={(e) => setChecked(e.target.checked)}
                      name="checkedB"
                      color="primary"
                      style={{ color: "white" }}
                      size={"small"}
                    />
                  }
                  label={<span style={{ fontSize: 14 }}>{"Ingat Saya"}</span>}
                />
                <input type="submit" value="Log In" style={{ marginTop: -3 }} />
                <div className="form-footer">
                  <div>
                    {ErrorMSG && (
                      <h5
                        style={{
                          color: "red",
                          backgroundColor: "white",
                          padding: 10,
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          borderRadius: 4,
                        }}
                      >
                        {ErrorMSG}
                      </h5>
                    )}
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default LoginPage;

const Wrapper = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  justify-content: center;
  align-items: center;
  input,
  button {
    font-family: "Muli", sans-serif, -apple-system, BlinkMacSystemFont,
      "Helvetica Neue", Helvetica, sans-serif;
    outline: none;
  }
  .Half {
    margin-top: 10px;
    /* background-color: white; */
  }
  .main-container {
    max-width: 900px;
    margin: 0 auto;
    min-width: 70vw;
  }

  a {
    color: inherit;
    outline: none;
    text-decoration: none;
  }

  a:hover {
    text-decoration: underline;
  }

  .form-container {
    max-width: 450px;
    width: 370px;
    margin: 0 auto;
  }

  .form-body {
    background-color: #ff8c1a;
    overflow: hidden;
    padding: 30px;
    color: #ffffff;
    border-radius: 3px;
    box-shadow: 0 20px 25px -5px rgba(0, 0, 0, 0.1),
      0 10px 10px -5px rgba(0, 0, 0, 0.04);
  }

  @media only screen and (max-width: 500px) {
    .form-body {
      padding: 50px 40px;
    }
  }

  @media only screen and (max-width: 455px) {
    .form-body {
      padding: 45px 30px;
    }
  }

  @media only screen and (max-width: 340px) {
    .form-body {
      padding: 30px 20px;
    }
  }

  .form-body .title {
    margin: 0;
    text-align: center;
    font-weight: normal;
  }
  .form-body .logo {
    margin: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
  }

  .social-login ul {
    list-style-type: none;
    margin: 30px 0;
    padding: 0;
    display: flex;
    flex-wrap: wrap;
  }

  .social-login ul li {
    flex: 1 auto;
  }

  .social-login ul li a {
    background-color: #56385a;
    border-radius: 3px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    padding: 15px;
    color: #e6e6e6;
    font-weight: bold;
    text-decoration: none;
    transition: background-color 0.3s;
  }

  .social-login ul li a::before {
    content: "";
    width: 30px;
    height: 30px;
    background-repeat: no-repeat;
    background-position: center;
    background-size: 30px;
    margin-right: 5px;
  }

  .social-login ul li a:hover {
    background-color: #fff199;
    color: #0e090e;
    box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
      0 4px 6px -2px rgba(0, 0, 0, 0.05);
  }

  li.google {
    margin-right: 10px;
  }

  li.google a::before {
    background-image: url("../images/google.png");
  }

  li.fb {
    margin-left: 10px;
  }

  li.fb a::before {
    margin: 0;
    background-image: url("../images/fb.png");
  }

  @media only screen and (max-width: 400px) {
    .social-login ul {
      flex-direction: column;
    }

    li.google,
    li.fb {
      margin: 0;
    }

    li.google {
      margin-bottom: 10px;
    }
  }

  ._or {
    text-align: center;
    margin-bottom: 20px;
    color: #d9d9d9;
  }

  .the-form {
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
  }

  .the-form label {
    margin-bottom: 5px;
    margin-top: 10px;
    color: #e6e6e6;
    font-weight: bold;
  }

  .the-form [type="text"],
  .the-form [type="password"] {
    /* padding: 10px; */
    font-size: 16px;
    /* background: rgba(0, 0, 0, 0.2); */
    background: white;
    border: 1px solid rgba(255, 255, 255, 0.1);
    border-radius: 5px;
    /* margin-bottom: 15px; */
    transition: background 0.3s;
    color: black;
    height: 15px;
  }

  .the-form [type="text"]::placeholder,
  .the-form [type="password"]::placeholder {
    color: grey;
    /* color: rgba(255, 255, 255, 0.3); */
  }

  .the-form [type="text"]:hover,
  .the-form [type="password"]:hover {
    /* background: rgba(0, 0, 0, 0.1); */
  }

  .the-form [type="text"]:focus,
  .the-form [type="password"]:focus {
    background: #ffffff;
    box-shadow: inset 0 2px 4px 0 rgba(0, 0, 0, 0.06);
    border-color: #4a304d;
    color: #222222;
  }

  .the-form [type="text"]:focus::placeholder,
  .the-form [type="password"]:focus::placeholder {
    color: #666666;
  }

  .the-form [type="submit"] {
    background: #00a4b3;
    border: 1px solid rgba(0, 0, 0, 0.1);
    /* padding: 10px; */
    font-size: 18px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 100px;
    cursor: pointer;
    margin-top: 20px;
    color: white;
    height: 40px;
    box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1),
      0 2px 4px -1px rgba(0, 0, 0, 0.06);
  }

  .the-form [type="submit"]:hover {
    opacity: 0.9;
  }

  .form-footer div {
    text-align: center;
    padding: 25px 20px;
    font-size: 18px;
    color: #e6e6e6;
  }

  .form-footer div a {
    color: #ffb37b;
  }
`;
