import React, { Fragment, useEffect, useState } from "react";
import { PageTitle } from "../../layout-components";
import DataTable from "layout-components/DataTable";

import {
  TableCell,
  TableRow,
  Menu,
  MenuItem,
  TextField,
  Button,
  Grid,
  CircularProgress,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { get, put } from "redux/features/slice";
import {
  Map,
  Menu as MenuIcon,
  CloudDownload,
  FilterList,
} from "@material-ui/icons";
import Swal from "sweetalert2";
import moment from "moment";
import { API_API_URL } from "utilities/BaseUrl";
import FileSaver from "file-saver";
import { ExampleWrapperSimple } from "../../layout-components";

export default function DataTableView() {
  const auth = useSelector((state) => state.auth);
  const searchTypeDelayTime = 500;
  let searchTypeDelay = null;
  const dispatch = useDispatch();
  const initState = {
    dataTable: {
      records: [],
      totalRecords: 0,
      search: null,
      rowsPerPage: 5,
      start: 0,
    },
    loading: {
      dataTable: false,
      exportData: false,
    },
  };
  const [dataTable, setDataTable] = useState(initState.dataTable);
  const [loading, setLoading] = useState(initState.loading);

  const [formData, setFormData] = useState({
    dateStart: moment().format("YYYY-MM-DD"),
    dateEnd: moment().format("YYYY-MM-DD"),
  });
  const getDataTable = (
    start = dataTable.start,
    length = dataTable.rowsPerPage,
    search = dataTable.search
  ) => {
    setLoading({ ...loading, dataTable: true });
    dispatch(
      get(
        `/api/orders/report/paid?start=${start}&length=${length}
        ${formData.dateStart ? "&start_date=" + formData.dateStart : ""}
        ${search ? "&search=" + search : ""}${
          formData.dateEnd ? "&end_date=" + formData.dateEnd : ""
        }`,
        (res) => {
          setDataTable({
            ...dataTable,
            records: res.data.data.data,
            totalRecords: res.data.data.count,
            rowsPerPage: length,
            start: start,
            page: start <= 0 ? 0 : Math.ceil(start / length),
            search,
          });
        },
        (err) => {
          alert(err);
        },
        () => {
          setLoading({ ...loading, dataTable: false });
        }
      )
    );
  };

  useEffect(() => {
    getDataTable();
  }, []);

  return (
    <Fragment>
      <PageTitle titleHeading="Report" titleDescription="Report Pembayaran" />
      <ExampleWrapperSimple>
        <Grid container style={{ marginBottom: 20 }} spacing={1}>
          <Grid item xs={12} sm={12} md={12}>
            <p style={{ fontSize: 14, fontWeight: 650 }}>Filter</p>
          </Grid>

          <Grid item xs={12} sm={6} md={4}>
            <p style={{ fontSize: 14, fontWeight: 500 }}>Tanggal Dari</p>
          </Grid>
          <Grid item xs={12} sm={6} md={8}>
            <TextField
              fullWidth
              id="dateStart"
              label="Tanggal Dari"
              type="date"
              defaultValue={formData.dateStart}
              value={formData.dateStart}
              onChange={(e) =>
                setFormData({ ...formData, dateStart: e.target.value })
              }
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={4}>
            <p style={{ fontSize: 14, fontWeight: 500 }}>Tanggal Sampai</p>
          </Grid>
          <Grid item xs={12} sm={6} md={8}>
            <TextField
              fullWidth
              id="dateEnd"
              label="Tanggal Sampai"
              type="date"
              defaultValue={formData.dateEnd}
              value={formData.dateEnd}
              onChange={(e) =>
                setFormData({ ...formData, dateEnd: e.target.value })
              }
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
        </Grid>

        <DataTable
          renderHeaderActions={
            <>
              <Grid container spacing={1}>
                <Grid item></Grid>
                <Grid item>
                  <Button
                    onClick={() => getDataTable(0)}
                    variant="contained"
                    size="medium"
                    color="primary"
                    disabled={loading.dataTable}
                    startIcon={
                      loading.dataTable ? (
                        <CircularProgress
                          size={20}
                          style={{ color: "white" }}
                        />
                      ) : (
                        <FilterList />
                      )
                    }
                  >
                    Terapkan Filter
                  </Button>
                </Grid>
              </Grid>
            </>
          }
          count={dataTable.totalRecords}
          page={dataTable.page}
          rows={dataTable.records}
          rowsPerPage={dataTable.rowsPerPage}
          headers={["Tgl Pembayaran", "Jenis Pembayaran", "Gross Sales"]}
          isLoading={loading.dataTable}
          renderRow={(d) => (
            <TableRow key={d.id}>
              <TableCell>
                {moment(d.date_transaction).format("YYYY-MM-DD")}
              </TableCell>
              <TableCell>{d.payment_type}</TableCell>
              <TableCell>{d.total_price}</TableCell>
            </TableRow>
          )}
          handleChangePage={(e, newPage) => {
            newPage = newPage * dataTable.rowsPerPage;
            getDataTable(newPage);
          }}
          handleChangeRowsPerPage={(e) => {
            getDataTable(0, parseInt(e.target.value));
          }}
          searchProps={{
            onChange: (event) => {
              const val = event.target?.value;
              if (searchTypeDelay != null) clearTimeout(searchTypeDelay);
              searchTypeDelay = setTimeout(() => {
                getDataTable(0, dataTable.rowsPerPage, val);
              }, searchTypeDelayTime);
            },
          }}
        />
      </ExampleWrapperSimple>
    </Fragment>
  );
}
