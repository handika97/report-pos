import React, { Fragment, useEffect, useState } from "react";
import { PageTitle } from "../../layout-components";
import DataTable from "layout-components/DataTable";

import {
  TableCell,
  TableRow,
  Button,
  CircularProgress,
  Grid,
  TextField,
  MenuItem,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { get } from "redux/features/slice";
import { CloudDownload, FilterList } from "@material-ui/icons";
import moment from "moment";
import FileSaver from "file-saver";
import { ExampleWrapperSimple } from "../../layout-components";

export default function DataTableView() {
  const auth = useSelector((state) => state.auth);
  const searchTypeDelayTime = 500;
  let searchTypeDelay = null;
  const dispatch = useDispatch();
  const initState = {
    dataTable: {
      records: [],
      totalRecords: 0,
      search: null,
      rowsPerPage: 5,
      start: 0,
    },
    loading: {
      dataTable: false,
      exportData: false,
    },
  };
  const [dataTable, setDataTable] = useState(initState.dataTable);
  const [loading, setLoading] = useState(initState.loading);
  const [branchList, setBranchList] = useState([]);
  const [agenList, setAgenList] = useState([]);
  const [subAgenList, setSubAgenList] = useState([]);
  const [formData, setFormData] = useState({
    id: null,
    verify_status: 1,
    dateStart: moment().format("YYYY-MM-DD"),
    dateEnd: moment().format("YYYY-MM-DD"),
    filter_divre: null,
    filter_main_agen: null,
    filter_id: null,
  });
  const getDataTable = (
    start = dataTable.start,
    length = dataTable.rowsPerPage,
    search = dataTable.search
  ) => {
    setLoading({ ...loading, dataTable: true });
    dispatch(
      get(
        `/report/agen?start=${start}&length=${length}${
          search ? "&search=" + search : ""
        }${formData.dateStart ? "&dateStart=" + formData.dateStart : ""}${
          formData.dateEnd ? "&dateEnd=" + formData.dateEnd : ""
        }${
          formData.filter_divre ? "&filter_divre=" + formData.filter_divre : ""
        }${formData.filter_id ? "&filter_id=" + formData.filter_id : ""}${
          formData.filter_main_agen
            ? "&filter_main_agen=" + formData.filter_main_agen
            : ""
        }${auth.data.role === "agent" ? "&id_agent=" + auth.data.id : ""}`,
        (res) => {
          setDataTable({
            ...dataTable,
            records: res.data.data.records,
            totalRecords: res.data.data.totalRecords,
            rowsPerPage: length,
            start: start,
            page: start <= 0 ? 0 : Math.ceil(start / length),
            search,
          });
        },
        (err) => {
          alert(err);
        },
        () => {
          setLoading({ ...loading, dataTable: false });
        }
      )
    );
  };
  const exportExcel = (
    start = dataTable.start,
    length = dataTable.rowsPerPage,
    search = dataTable.search
  ) => {
    setLoading({ ...loading, exportData: true });
    dispatch(
      get(
        `/report/agen/exportExcel?start=${start}&length=${length}${
          search ? "&search=" + search : ""
        }${formData.dateStart ? "&dateStart=" + formData.dateStart : ""}${
          formData.dateEnd ? "&dateEnd=" + formData.dateEnd : ""
        }${
          formData.filter_divre ? "&filter_divre=" + formData.filter_divre : ""
        }${formData.filter_id ? "&filter_id=" + formData.filter_id : ""}${
          formData.filter_main_agen
            ? "&filter_main_agen=" + formData.filter_main_agen
            : ""
        }${auth.data.role === "agent" ? "&id_agent=" + auth.data.id : ""}`,
        (res) => {
          const dirtyFileName = res.headers["content-disposition"];
          console.log(res);
          const regex = /filename[^;=\n]*=(?:(\\?['"])(.*?)\1|(?:[^\s]+'.*?')?([^;\n]*))/;
          const fileName = dirtyFileName.match(regex)[3];

          const blob = new Blob([res.data], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          FileSaver.saveAs(blob, fileName);
        },
        (err) => {
          alert(err);
        },
        () => {
          setLoading({ ...loading, exportData: false });
        },
        {
          responseType: "arraybuffer",
        }
      )
    );
  };
  const getBranch = () =>
    dispatch(
      get(
        "/utils/branch",
        (res) => {
          setBranchList([{ label: "All", value: "" }, ...res.data.data]);
        },
        (err) => {
          console.log(err);
        }
      )
    );
  const getAgen = () =>
    dispatch(
      get(
        `/report/filterAgen?filter_role=1&filter_divre=${formData.filter_divre}`,
        (res) => {
          setAgenList([{ label: "All", value: "" }, ...res.data.data.records]);
        },
        (err) => {
          console.log(err);
        }
      )
    );
  const getSubAgen = () =>
    dispatch(
      get(
        `/report/filterAgen?filter_role=2&filter_divre=${
          formData.filter_divre ? formData.filter_divre : ""
        }&filter_main_agen=${
          auth.data.role == "agent" ? auth.data.id : formData.filter_main_agen
        }`,
        (res) => {
          setSubAgenList([
            { label: "All", value: "" },
            ...res.data.data.records,
          ]);
        },
        (err) => {
          console.log(err);
        }
      )
    );
  useEffect(() => {
    getAgen();
  }, [formData.filter_divre]);
  useEffect(() => {
    getSubAgen();
  }, [formData.filter_main_agen]);

  useEffect(() => {
    getDataTable();
    getBranch();
  }, []);

  return (
    <Fragment>
      <PageTitle titleHeading="Report" titleDescription="Agen" />
      <ExampleWrapperSimple>
        <Grid container style={{ marginBottom: 20 }} spacing={1}>
          <Grid item xs={12} sm={12} md={12}>
            <p style={{ fontSize: 14, fontWeight: 650 }}>Filter</p>
          </Grid>

          <Grid item xs={12} sm={6} md={4}>
            <p style={{ fontSize: 14, fontWeight: 500 }}>Tanggal Dari</p>
          </Grid>
          <Grid item xs={12} sm={6} md={8}>
            <TextField
              fullWidth
              id="dateStart"
              label="Tanggal Dari"
              type="date"
              defaultValue={formData.dateStart}
              value={formData.dateStart}
              onChange={(e) =>
                setFormData({ ...formData, dateStart: e.target.value })
              }
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={4}>
            <p style={{ fontSize: 14, fontWeight: 500 }}>Tanggal Sampai</p>
          </Grid>
          <Grid item xs={12} sm={6} md={8}>
            <TextField
              fullWidth
              id="dateEnd"
              label="Tanggal Sampai"
              type="date"
              defaultValue={formData.dateEnd}
              value={formData.dateEnd}
              onChange={(e) =>
                setFormData({ ...formData, dateEnd: e.target.value })
              }
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>

          {auth.data.role != "agent" && (
            <>
              {" "}
              <Grid item xs={12} sm={6} md={4}>
                <p style={{ fontSize: 14, fontWeight: 500 }}>Divre</p>
              </Grid>
              <Grid item xs={12} sm={6} md={8}>
                <TextField
                  fullWidth
                  id="filter_divre"
                  select
                  value={formData.filter_divre}
                  variant="outlined"
                  size="small"
                  onChange={(e) =>
                    setFormData({ ...formData, filter_divre: e.target.value })
                  }
                >
                  {branchList.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <p style={{ fontSize: 14, fontWeight: 500 }}>Agen</p>
              </Grid>
              <Grid item xs={12} sm={6} md={8}>
                <TextField
                  fullWidth
                  id="filter_main_agen"
                  select
                  value={formData.filter_main_agen}
                  variant="outlined"
                  size="small"
                  onChange={(e) =>
                    setFormData({
                      ...formData,
                      filter_main_agen: e.target.value,
                    })
                  }
                >
                  {agenList.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
            </>
          )}
          <Grid item xs={12} sm={6} md={4}>
            <p style={{ fontSize: 14, fontWeight: 500 }}>Sub Agen</p>
          </Grid>
          <Grid item xs={12} sm={6} md={8}>
            <TextField
              fullWidth
              id="filter_id"
              select
              value={formData.filter_id}
              variant="outlined"
              size="small"
              onChange={(e) =>
                setFormData({ ...formData, filter_id: e.target.value })
              }
            >
              {subAgenList.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
        </Grid>

        <DataTable
          renderHeaderActions={
            <>
              <Grid container spacing={1}>
                <Grid item>
                  <Button
                    onClick={() => {
                      exportExcel();
                    }}
                    variant="contained"
                    size="medium"
                    disabled={loading.exportData}
                    style={{ backgroundColor: "green", color: "white" }}
                    startIcon={
                      loading.exportData ? (
                        <CircularProgress
                          size={20}
                          style={{ color: "white" }}
                        />
                      ) : (
                        <CloudDownload />
                      )
                    }
                  >
                    {" "}
                    Export Excel
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    onClick={() => getDataTable(0)}
                    variant="contained"
                    size="medium"
                    color="primary"
                    disabled={loading.dataTable}
                    startIcon={
                      loading.dataTable ? (
                        <CircularProgress
                          size={20}
                          style={{ color: "white" }}
                        />
                      ) : (
                        <FilterList />
                      )
                    }
                  >
                    Terapkan Filter
                  </Button>
                </Grid>
              </Grid>
            </>
          }
          count={dataTable.totalRecords}
          page={dataTable.page}
          rows={dataTable.records}
          rowsPerPage={dataTable.rowsPerPage}
          headers={[
            "Dari Tgl",
            "Sampai Tgl",
            "ID Agen Utama",
            "Nama Agen Utama",
            "ID Sub Agen",
            "Nama Sub Agen",
            "Area",
            "Register",
            "Login",
            "Verifikasi",
            "Order",
            "Transaksi",
            "Aktifitas Deal",
          ]}
          isLoading={loading.dataTable}
          renderRow={(d) => (
            <TableRow key={d.id}>
              <TableCell>{formData.dateStart}</TableCell>
              <TableCell>{formData.dateEnd}</TableCell>
              <TableCell>{d.id_agent}</TableCell>
              <TableCell>{d.main_agent_name}</TableCell>
              <TableCell>{d.id}</TableCell>
              <TableCell>{d.name}</TableCell>
              <TableCell>{d.prov_name + " - " + d.kota_name}</TableCell>
              <TableCell>{d.mitra_teregister}</TableCell>
              <TableCell>{d.mitra_login}</TableCell>
              <TableCell>{d.mitra_terverifikasi}</TableCell>
              <TableCell>{d.mitra_order}</TableCell>
              <TableCell>
                Rp{parseInt(d.mitra_transaksi).toLocaleString("id")}
              </TableCell>
              <TableCell>{d.total_deal_activity}</TableCell>
            </TableRow>
          )}
          handleChangePage={(e, newPage) => {
            newPage = newPage * dataTable.rowsPerPage;
            getDataTable(newPage);
          }}
          handleChangeRowsPerPage={(e) => {
            getDataTable(0, parseInt(e.target.value));
          }}
          searchProps={{
            onChange: (event) => {
              const val = event.target?.value;
              if (searchTypeDelay != null) clearTimeout(searchTypeDelay);
              searchTypeDelay = setTimeout(() => {
                getDataTable(0, dataTable.rowsPerPage, val);
              }, searchTypeDelayTime);
            },
          }}
        />
      </ExampleWrapperSimple>
    </Fragment>
  );
}
