import React from "react";
import styled, { css } from "styled-components";
import Select from "react-select";

const selectStyles = {
  control: (provided) => ({
    ...provided,
    boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",
    border: "0.2px solid #b0bec5",
    borderRadius: "0.4rem",
    padding: "2px 2px",
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    // const color = chroma(data.color);
    return {
      ...styles,
      backgroundColor: isFocused ? "#999999" : null,
      color: "#333333",
    };
  },
  // menu: () => ({
  //   boxShadow: "inset 0 1px 0 rgba(0, 0, 0, 0.1)",
  //   backgroundColor: "white",
  // }),
};

export default function DropDown({
  Data,
  full = false,
  value,
  onChange = () => {},
  isSearchable,
  data,
  label,
  noPadding,
  disabled,
  Action,
  onInputChange = () => {},
  ...rest
}) {
  const [selected, setSelected] = React.useState("");
  return (
    <StyledDiv>
      <Select
        options={Data}
        disabled={true}
        onInputChange={(e) => onInputChange(e)}
        onChange={(e) => {
          console.log(e);
          onChange(e);
          setSelected(e);
        }}
        value={value && !selected ? value : selected}
        styles={selectStyles}
        isSearchable={isSearchable}
      />
    </StyledDiv>
  );
}
const StyledDiv = styled.div`
  .SelectPicker {
    background-color: transparent;
    border: 1px var(--border-input-color) solid;
    border-radius: 2px;
    max-width: 150px;
    box-sizing: content-box;
    ${({ full }) =>
      full &&
      css`
        width: 100%;
        max-width: 100%;
      `}
    select {
      border: none;
      outline: none;
      width: 100%;
      height: 100%;
      padding: ${({ noPadding }) => (noPadding ? "0" : "10px 12px")};
      color: grey;
      border-radius: 2px;
      transition: 0.2s;
    }
    select:focus {
      box-shadow: 0 0 0 1px var(--border-primary-color);
    }
  }
  .ActionSelectPicker {
    background-color: var(--background-dropdown-action);
    border: 1px var(--background-dropdown-action) solid;
    height: 30px;
    max-width: 75px;
    border-radius: 2px;
    select {
      border: none;
      outline: none;
      width: 100%;
      height: 100%;
      padding: ${({ noPadding }) => (noPadding ? "0" : "10px 12px")};
      color: white;
      border-radius: 2px;
      background-color: var(--background-dropdown-action);
      transition: 0.2s;
    }
    .option {
      background-color: white;
      color: black;
    }
  }
`;
