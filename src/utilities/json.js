import React from "react";

import {
  FaAccusoft,
  FaDatabase,
  FaFileDownload,
  FaFileUpload,
  FaCopy,
  FaRegListAlt,
  FaUserFriends,
} from "react-icons/fa";
import { IoMdNotifications } from "react-icons/io";

const iconSize = 17;
const color = "white";

export const SidebarMenu = [
  {
    title: "",
    Menu: [
      {
        icon: <IoMdNotifications size={18} color="white" />,
        Label: "Notifikasi",
        path: "/notification",
      },
      {
        icon: <FaFileDownload size={iconSize} color="white" />,
        Label: "Entry",
        path: "/entry",
        subMenu: [
          {
            label: "Create Entry",
            path: "/entry/list",
          },
          {
            label: "Approve Entry",
            path: "/entry/approval",
          },
        ],
      },
      {
        icon: <FaFileUpload size={iconSize} color="white" />,
        Label: "Exit",
        path: "/exit",
        subMenu: [
          {
            label: "Create Exit",
            path: "/exit/list",
          },
          {
            label: "Approve Exit",
            path: "/exit/approval",
          },
        ],
      },
      {
        icon: <FaDatabase size={iconSize} color="white" />,
        Label: "Master Item",
        path: "/masterdata",
      },
      {
        icon: <FaRegListAlt size={iconSize} color="white" />,
        Label: "Tracking",
        path: "/tracking",
      },
      {
        icon: <FaCopy size={iconSize} color="white" />,
        Label: "Report",
        path: "/report",
      },
      {
        icon: <FaUserFriends size={iconSize} color="white" />,
        Label: "Users",
        path: "/users",
      },
    ],
  },
];
export const SidebarManagerMenu = [
  {
    title: "",
    Menu: [
      {
        icon: <IoMdNotifications size={18} color="white" />,
        Label: "Notifikasi",
        path: "/notification",
      },
      {
        icon: <FaFileDownload size={iconSize} color="white" />,
        Label: "Entry",
        path: "/entry",
        subMenu: [
          {
            label: "Approve Entry",
            path: "/entry/approval",
          },
        ],
      },
      {
        icon: <FaFileUpload size={iconSize} color="white" />,
        Label: "Exit",
        path: "/exit",
        subMenu: [
          {
            label: "Approve Exit",
            path: "/exit/approval",
          },
        ],
      },
    ],
  },
];
