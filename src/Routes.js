import React, { lazy, Suspense } from "react";
import { Switch, Route, Redirect, useLocation } from "react-router-dom";
import { AnimatePresence, motion } from "framer-motion";

import { ThemeProvider } from "@material-ui/styles";

import MuiTheme from "./theme";

// Layout Blueprints

import { LeftSidebar, PresentationLayout } from "./layout-blueprints";

// Example Pages

import { useDispatch, useSelector } from "react-redux";
const reportMenu = lazy(() => import("./page/report/reportMenu"));
const reportComposition = lazy(() => import("./page/report/reportComposition"));
const reportPaid = lazy(() => import("./page/report/reportPaid"));

const Routes = ({ match }) => {
  console.log(match);
  const auth = useSelector((state) => state.auth);

  const location = useLocation();

  const pageVariants = {
    initial: {
      opacity: 0,
      scale: 0.99,
    },
    in: {
      opacity: 1,
      scale: 1,
    },
    out: {
      opacity: 0,
      scale: 1.01,
    },
  };

  const pageTransition = {
    type: "tween",
    ease: "anticipate",
    duration: 0.4,
  };

  return (
    <ThemeProvider theme={MuiTheme}>
      <AnimatePresence>
        <Suspense
          fallback={
            <div className="d-flex align-items-center vh-100 justify-content-center text-center font-weight-bold font-size-lg py-3">
              <div className="w-50 mx-auto">Please wait loading!!!!</div>
            </div>
          }
        >
          <Switch>
            <LeftSidebar>
              <Route path={`${match.url}/report/menu`} component={reportMenu} />
              <Route
                path={`${match.url}/report/composition`}
                component={reportComposition}
              />
              <Route path={`${match.url}/report/paid`} component={reportPaid} />
              <Redirect exact to={`${match.url}/report/menu`} />
            </LeftSidebar>
          </Switch>
        </Suspense>
      </AnimatePresence>
    </ThemeProvider>
  );
};

export default Routes;
